package com.hasscript.mhrs;

import com.hasscript.mhrs.MhrsSampleApp;
import com.hasscript.mhrs.config.EmbeddedMongo;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Base composite annotation for integration tests.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringBootTest(classes = MhrsSampleApp.class)
@EmbeddedMongo
public @interface IntegrationTest {
}
