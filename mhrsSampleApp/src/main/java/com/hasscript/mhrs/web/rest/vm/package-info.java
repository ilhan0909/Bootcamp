/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hasscript.mhrs.web.rest.vm;
